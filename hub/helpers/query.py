import random

from redis import StrictRedis


def get_recipients(receiver_id, location, message_type, redis: StrictRedis):
    """
    Returns set of currently related recipients
    """
    device_types = {'device_type:{}'.format(device_type.decode('utf8'))
                    for device_type in redis.smembers('message_type:{}'.format(message_type))}
    recipients = set()

    if len(device_types):
        if location or receiver_id:
            tmp_store = 'tmp_store:{}'.format(random.random())
            redis.sunionstore(tmp_store, *device_types)

            if receiver_id:
                if redis.sismember(tmp_store, receiver_id):
                    recipients = {receiver_id.encode('utf8')}
            elif location:
                recipients = redis.sinter(tmp_store, 'location:{}'.format(location))

            del redis[tmp_store]
        else:
            # TODO: union all sender locations (not all locations at all)
            # TODO: tests
            recipients = redis.sunion(*device_types)

    return {device_id.decode('utf8') for device_id in recipients}


def is_device_id_present(device_id: str, redis: StrictRedis):
    """
    Validates precence of `device_id` in stored devices.
    """
    device_types = redis.keys('device_type:*')

    if not len(device_types):
        return False

    tmp_store = 'tmp_store:{}'.format(random.random())
    redis.sunionstore(tmp_store, *device_types)

    result = redis.sismember(tmp_store, device_id)
    del redis[tmp_store]

    return result
