from redis import StrictRedis
from sanic import Sanic

from utils.crypting import get_rsa_keys
from utils.device_id import get_device_id_from_key_pub


class HubBase:
    def __init__(self, host, port, redis: StrictRedis=None, log_config=None):
        self.app = Sanic(__name__, log_config=log_config)
        self.redis = redis

        self.host = host.rstrip('/')
        self.port = port

        self.url = 'http://{}:{}/'.format(self.host, self.port)
        self.ws_url = 'ws://{}:{}/ws'.format(self.host, self.port)

        self.key_pub, self.key = get_rsa_keys(pair_name='hub')
        self.device_id = get_device_id_from_key_pub(self.key_pub)
