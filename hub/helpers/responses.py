from sanic import response


ERROR_INVALID_FORMAT = response.text('Error: invalid format', status=400)
SUCCESS = response.text('OK')
