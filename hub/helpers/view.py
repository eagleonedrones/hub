from sanic import response
from sanic.request import Request
from sanic.router import RouteExists
from websockets import WebSocketCommonProtocol

from hub.helpers.base import HubBase


class BaseViewWrapper:
    """
        Say something about this view...
    """
    url = None
    handler = None
    method = 'POST'

    def __init__(self, hub: HubBase):
        self.hub = hub
        self.register()

    def _docs_handler(self, request):
        return response.text(self.__doc__)

    def register(self):
        try:
            self.hub.app.add_route(self._docs_handler, '{}/doc'.format(self.url.rstrip('/')), methods=['GET'])
        except RouteExists:
            pass


class ViewWrapper(BaseViewWrapper):
    method = 'POST'

    def handler(self, request: Request):
        raise NotImplementedError()

    def register(self):
        super(ViewWrapper, self).register()
        self.hub.app.add_route(self.handler, self.url, methods=[self.method.upper()])
        # try:
        #     self.hub.app.add_route(self.handler, self.url, methods=[self.method.upper()])
        # except RouteExists:
        #     pass


class WsViewWrapper(BaseViewWrapper):
    def handler(self, request: Request, websocket: WebSocketCommonProtocol):
        raise NotImplementedError()

    def register(self):
        self.hub.app.add_route(self._docs_handler, '{}/doc'.format(self.url.rstrip('/')), methods=['GET'])
        self.hub.app.add_websocket_route(self.handler, self.url)
        # try:
        #     self.hub.app.add_route(self._docs_handler, '{}/doc'.format(self.url.rstrip('/')), methods=['GET'])
        #     self.hub.app.add_websocket_route(self.handler, self.url)
        # except RouteExists:
        #     pass
