from client.client import HubClient
from hub import HubBase
from utils.crypting import get_rsa_keys


class DummyDevice(HubClient):
    def __init__(self, hub: HubBase, rsa_keys_name: str):
        self.device_alias = rsa_keys_name
        key_pub, key = get_rsa_keys(pair_name=rsa_keys_name)
        super(DummyDevice, self).__init__(hub.ws_url, key_pub, key, hub.key_pub)
