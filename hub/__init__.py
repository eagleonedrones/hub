from sanic.websocket import WebSocketProtocol

from hub.handlers import *  # DO NOT REMOVE - imports all handlers to register them
from hub.helpers.base import HubBase
from hub.helpers.view import BaseViewWrapper


class Hub(HubBase):
    def __init__(self, *args, **kwargs):
        super(Hub, self).__init__(*args, **kwargs)
        self.server = None

    def register_handlers(self):
        for _sub in BaseViewWrapper.__subclasses__():
            for handler in _sub.__subclasses__():
                self.register_handler(handler)

    def register_handler(self, handler):
        handler(self)

    def start(self, **kwargs):
        self.register_handlers()
        self.app.run(host=self.host, port=self.port, protocol=WebSocketProtocol, **kwargs)

    async def debug(self):
        self.register_handlers()
        self.server = await self.app.create_server(host=self.host, port=self.port, debug=True)

    async def close(self):
        if self.server:
            self.server.close()
            self.server = None
