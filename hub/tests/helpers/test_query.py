import asyncio
import unittest

from client.input_formats import MessageToHub

from hub.handlers import SetDeliveranceView, SetDeviceView
from hub.helpers.query import get_recipients, is_device_id_present
from hub.tests import SetHubWriterTestCase
from utils.aiotools import run_in_sync
from utils.testing import with_setup_and_finish


class TestCaseWithSomeDevicesAndDeliverancesDefined(SetHubWriterTestCase):
    async def _initiate_all(self):
        await asyncio.gather(self._initiate_deliverances(),
                             self._initiate_devices())

    async def _initiate_deliverances(self):
        deliverances = [
            {
                'message_type': 'alert',
                'device_types': ['drone', 'mobile'],
            },
            {
                'message_type': 'info',
                'device_types': ['drone', 'mobile', 'hub'],
            },
            {
                'message_type': 'healthcheck',
                'device_types': ['hub'],
            },
        ]
        for deliverance in deliverances:
            response = await self.ugly_post_wrapper(SetDeliveranceView.url, json=deliverance)
            self.assertEqual('OK', response)

    async def _initiate_devices(self):
        devices = [
            {
                'id': 'drone_co_1',
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': 'drone_co_2',
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': 'drone_jd_1',
                'device_type': 'drone',
                'locations': ['jd'],
            },
            {
                'id': 'mobile_jd',
                'device_type': 'mobile',
                'locations': ['jd'],
            },
            {
                'id': 'mobile_co_jd',
                'device_type': 'mobile',
                'locations': ['jd', 'co'],
            },
        ]
        for device in devices:
            response = await self.ugly_post_wrapper(SetDeviceView.url, json=device)
            self.assertEqual('OK', response)


class InvalidMessageTestCase(unittest.TestCase):
    def test_provide_location_and_receiver_id_will_fail(self):
        with self.assertRaises(ValueError):
            MessageToHub(message_type='whatever', data={}, location='lc', receiver_id='id')


class GlobalMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='alert', data={})
        self.assertSetEqual({'drone_co_1', 'drone_co_2', 'drone_jd_1', 'mobile_jd', 'mobile_co_jd'},
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='healthcheck', data={})
        self.assertSetEqual(set(), get_recipients(message.receiver_id,
                                                  message.location,
                                                  message.message_type,
                                                  self.redis))


class LocationBasedMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='alert', data={}, location='co')
        self.assertSetEqual({'drone_co_1', 'drone_co_2', 'mobile_co_jd'},
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))

        message = MessageToHub(message_type='alert', data={}, location='jd')
        self.assertSetEqual({'drone_jd_1', 'mobile_jd', 'mobile_co_jd'},
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='alert', data={}, location='jinde')
        self.assertSetEqual(set(),
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))


class DirectMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='alert', data={}, receiver_id='drone_co_1')
        self.assertSetEqual({'drone_co_1'},
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await self._initiate_all()

        message = MessageToHub(message_type='alert', data={}, receiver_id='whatever')
        self.assertSetEqual(set(),
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))

        message = MessageToHub(message_type='whatever', data={}, receiver_id='drone_co_1')
        self.assertSetEqual(set(),
                            get_recipients(message.receiver_id,
                                           message.location,
                                           message.message_type,
                                           self.redis))


class DevicePresenceTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_device_id_should_be_presented(self):
        await self._initiate_all()

        self.assertTrue(is_device_id_present('drone_co_1', self.redis))
        self.assertTrue(is_device_id_present('drone_co_2', self.redis))

    @run_in_sync
    @with_setup_and_finish
    async def test_device_id_should_not_be_presented(self):
        await self._initiate_all()

        self.assertFalse(is_device_id_present('whatever', self.redis))

    @run_in_sync
    @with_setup_and_finish
    async def test_returns_false_when_no_devices_are_set_up(self):
        self.assertFalse(is_device_id_present('drone_co_1', self.redis))
