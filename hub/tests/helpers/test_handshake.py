import settings
from client.input_formats import MessageFromHub
from client.responses import MESSAGE_CONFIRMATION, MESSAGE_PING, MESSAGE_PING_DATA, MESSAGE_PONG_DATA, MESSAGE_PONG, \
    MESSAGE_UNDEFINED_CLIENT, MESSAGE_UNDEFINED_CLIENT_DATA
from hub import Hub, SetDeviceView
from hub.helpers.base import HubBase
from hub.helpers.dummy_device import DummyDevice
from hub.tests import SetHubWriterTestCase
from utils.aiotools import sleep_until, run_in_sync, run_cascade
from utils.crypting import import_key_pub, decrypt
from utils.testing import WsProxy, with_setup_and_finish


class HubMock(HubBase):
    def __init__(self, hub: HubBase, ws_url):
        super(HubMock, self).__init__(hub.host, hub.port)

        self.key_pub = hub.key_pub
        self.key = hub.key
        self.device_id = hub.device_id
        self.ws_url = ws_url


class CommunicationTestCase(SetHubWriterTestCase):
    def setUp(self):
        super(CommunicationTestCase, self).setUp()

        self.hub = Hub(host=settings.TEST_APP_HOST, port=settings.TEST_APP_PORT, redis=self.redis)
        self.ws_proxy = WsProxy(self.hub.ws_url, host=settings.TEST_APP_HOST, port=settings.TEST_APP_PORT + 1)
        self.client = DummyDevice(HubMock(self.hub, self.ws_proxy.public_url), 'test_pair_1')

        self._setup = run_cascade(
            self._setup,
            self.ws_proxy.serve(),
            self.client.connect(),
        )
        self._finish = run_cascade(
            self.client.disconnect(),
            self.ws_proxy.close(),
            self._finish,
        )

    async def _initiate_all(self):
        response = await self.ugly_post_wrapper(SetDeviceView.url, json={
            'id': self.client.device_id,
            'device_type': 'random',
            'locations': [],
        })
        self.assertEqual('OK', response)

    @run_in_sync
    @with_setup_and_finish
    async def test_message_confirmation(self):
        await self._initiate_all()

        await self.client.send('alert', {}, _wait_for_confirmation=True)

        self.assertEqual(1, len(self.client.received_messages))

        confirmation = self.client.received_messages[0]
        self.assertEqual(MESSAGE_CONFIRMATION, confirmation.message_type)
        self.assertEqual(self.client.sent_messages[0].emitted, confirmation.data.emitted)
        self.assertEqual(self.hub.device_id, confirmation.sender_id)

    @run_in_sync
    @with_setup_and_finish
    async def test_ping_pong(self):
        await self._initiate_all()

        await self.client.send(MESSAGE_PING, MESSAGE_PING_DATA)
        await sleep_until(lambda: len(self.client.received_messages))

        pong = self.client.received_messages[0]
        self.assertIsInstance(pong, MessageFromHub)
        self.assertEqual(MESSAGE_PONG, pong.message_type)
        self.assertDictEqual(MESSAGE_PONG_DATA, pong.data.plain)

    @run_in_sync
    @with_setup_and_finish
    async def test_handshake(self):
        await self._initiate_all()

        await sleep_until(lambda: len(self.ws_proxy.sent_messages) == 4)

        self.assertEqual(3, len(self.ws_proxy.received_messages))
        self.assertEqual(self.hub.key_pub, import_key_pub(self.ws_proxy.received_messages[0]))
        secret = decrypt(self.ws_proxy.received_messages[1], self.client.key)
        self.assertEqual('secret', secret[:6])
        self.assertEqual('Welcome!', decrypt(self.ws_proxy.received_messages[2], self.client.key))

        self.assertEqual(4, len(self.ws_proxy.sent_messages))
        self.assertEqual(self.client.key_pub, import_key_pub(self.ws_proxy.sent_messages[0]))
        self.assertEqual('What\'s the question?', decrypt(self.ws_proxy.sent_messages[1], self.hub.key))
        self.assertEqual(secret, decrypt(self.ws_proxy.sent_messages[2], self.hub.key))
        self.assertEqual('Hi!', decrypt(self.ws_proxy.sent_messages[3], self.hub.key))

    @run_in_sync
    async def skip_test_unsuccessful_handshake(self):
        # TODO: test, kdy zamítneme handshake
        pass

    @run_in_sync
    @with_setup_and_finish
    async def test_unregistered_device_receive_authorization_request(self):
        await sleep_until(lambda: len(self.client.received_messages) == 1)

        auth_required_msg = self.client.received_messages[0]
        self.assertEqual(MESSAGE_UNDEFINED_CLIENT, auth_required_msg.message_type)
        self.assertEqual(MESSAGE_UNDEFINED_CLIENT_DATA, auth_required_msg.data.plain)
        self.assertEqual(self.hub.device_id, auth_required_msg.sender_id)
