import aiohttp

import settings
from hub import Hub
from tests.helpers import TestCaseWithRedis
from utils.aiotools import run_cascade, run_in_sync


class SetHubWriterTestCase(TestCaseWithRedis):
    def setUp(self):
        super(SetHubWriterTestCase, self).setUp()

        self.hub = Hub(host=settings.TEST_APP_HOST, port=settings.TEST_APP_PORT, redis=self.redis)

        self._setup = run_cascade(
            self.hub.debug(),
        )
        self._finish = run_cascade(
            self.hub.close(),
        )

    @run_in_sync
    async def tearDown(self):
        try:
            await self._finish
        except RuntimeError:
            pass
        super(SetHubWriterTestCase, self).tearDown()

    def ugly_post_wrapper(self, url, *args, **kwargs):
        return self._ugly_request_wrapper('post')(url, *args, **kwargs)

    def ugly_get_wrapper(self, url, *args, **kwargs):
        return self._ugly_request_wrapper('get')(url, *args, **kwargs)

    def _ugly_request_wrapper(self, method):
        async def _request_wrapper(url, *args, **kwargs):
            url = '{}/{}'.format(self.hub.url.rstrip('/'), url.lstrip('/'))
            async with aiohttp.ClientSession() as session:
                async with getattr(session, method)(url, *args, **kwargs) as resp:
                    return await resp.text()

        return _request_wrapper
