from hub import PubKeyView
from hub.tests import SetHubWriterTestCase
from utils.aiotools import run_in_sync
from utils.crypting import export_key_pub, import_key_pub
from utils.testing import with_setup_and_finish


class PublicKeyTestCase(SetHubWriterTestCase):
    @run_in_sync
    @with_setup_and_finish
    async def test_shown_public_key_is_same_as_key_from_runtime(self):
        public_key_string = await self.ugly_get_wrapper(PubKeyView.url)
        self.assertEqual(export_key_pub(self.hub.key_pub), public_key_string)
        self.assertEqual(self.hub.key_pub, import_key_pub(public_key_string))
