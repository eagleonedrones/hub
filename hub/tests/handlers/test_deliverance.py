from hub.handlers import SetDeliveranceView, RemoveDeliveranceView
from hub.tests import SetHubWriterTestCase
from utils.aiotools import run_in_sync
from utils.testing import with_setup_and_finish


class SetDeliveranceTestCase(SetHubWriterTestCase):
    @run_in_sync
    @with_setup_and_finish
    async def test_set_new_deliverance_invalid_input(self):
        response = await self.ugly_post_wrapper(SetDeliveranceView.url, json={'jina': 'data'})
        self.assertEqual('Error: invalid format', response)

    async def _initiate_deliverances(self):
        deliverances = [
            {
                'message_type': 'alert',
                'device_types': ['drone', 'mobile'],
            },
            {
                'message_type': 'info',
                'device_types': ['drone', 'mobile', 'hub'],
            },
            {
                'message_type': 'healthcheck',
                'device_types': ['hub'],
            },
        ]
        for deliverance in deliverances:
            response = await self.ugly_post_wrapper(SetDeliveranceView.url, json=deliverance)
            self.assertEqual('OK', response)

        return deliverances

    @run_in_sync
    @with_setup_and_finish
    async def test_set_new_deliverance(self):
        await self._initiate_deliverances()

        self.assertSetEqual({b'message_type:info', b'message_type:healthcheck', b'message_type:alert'},
                            set(self.redis.keys('message_type:*')))

        self.assertSetEqual({b'drone', b'mobile'}, self.redis.smembers('message_type:alert'))
        self.assertSetEqual({b'drone', b'mobile', b'hub'}, self.redis.smembers('message_type:info'))
        self.assertSetEqual({b'hub'}, self.redis.smembers('message_type:healthcheck'))

    @run_in_sync
    @with_setup_and_finish
    async def test_override_deliverance(self):
        deliverance = (await self._initiate_deliverances())[0]
        deliverance['device_types'] = ['hub']

        response = await self.ugly_post_wrapper(SetDeliveranceView.url, json=deliverance)
        self.assertEqual('OK', response)

        self.assertSetEqual({b'message_type:info', b'message_type:healthcheck', b'message_type:alert'},
                            set(self.redis.keys('message_type:*')))

        self.assertSetEqual({b'hub'}, self.redis.smembers('message_type:alert'))
        self.assertSetEqual({b'drone', b'mobile', b'hub'}, self.redis.smembers('message_type:info'))
        self.assertSetEqual({b'hub'}, self.redis.smembers('message_type:healthcheck'))

    @run_in_sync
    @with_setup_and_finish
    async def test_delete_deliverance(self):
        deliverance = (await self._initiate_deliverances())[0]
        response = await self.ugly_post_wrapper(RemoveDeliveranceView.url,
                                                json={'message_type': deliverance['message_type']})
        self.assertEqual('OK', response)

        self.assertSetEqual({b'message_type:info', b'message_type:healthcheck'},
                            set(self.redis.keys('message_type:*')))

        self.assertSetEqual(set(), self.redis.smembers('message_type:alert'))
        self.assertSetEqual({b'drone', b'mobile', b'hub'}, self.redis.smembers('message_type:info'))
        self.assertSetEqual({b'hub'}, self.redis.smembers('message_type:healthcheck'))

    @run_in_sync
    @with_setup_and_finish
    async def test_delete_non_existing_deliverance(self):
        response = await self.ugly_post_wrapper(RemoveDeliveranceView.url, json={'message_type': 'non-existing-id'})
        self.assertEqual('OK', response)
