from hub.handlers import SetDeviceView, RemoveDeviceView
from hub.tests import SetHubWriterTestCase
from utils.aiotools import run_in_sync
from utils.testing import with_setup_and_finish


class SetDeviceTestCase(SetHubWriterTestCase):
    @run_in_sync
    @with_setup_and_finish
    async def test_set_new_device_invalid_input(self):
        response = await self.ugly_post_wrapper(SetDeviceView.url, json={'jina': 'data'})
        self.assertEqual('Error: invalid format', response)

    async def _initiate_devices(self):
        devices = [
            {
                'id': 'drone_co_1',
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': 'drone_co_2',
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': 'mobile',
                'device_type': 'mobile',
                'locations': ['jd', 'co'],
            },
        ]
        for device in devices:
            response = await self.ugly_post_wrapper(SetDeviceView.url, json=device)
            self.assertEqual('OK', response)

        return devices

    @run_in_sync
    @with_setup_and_finish
    async def test_set_new_device(self):
        await self._initiate_devices()

        self.assertSetEqual({b'location:co', b'location:jd'}, set(self.redis.keys('location:*')))
        self.assertSetEqual({b'device_type:drone', b'device_type:mobile'}, set(self.redis.keys('device_type:*')))

        self.assertSetEqual({b'drone_co_1', b'drone_co_2', b'mobile'}, self.redis.smembers('location:co'))
        self.assertSetEqual({b'mobile'}, self.redis.smembers('location:jd'))

        self.assertSetEqual({b'drone_co_1', b'drone_co_2'}, self.redis.smembers('device_type:drone'))
        self.assertSetEqual({b'mobile'}, self.redis.smembers('device_type:mobile'))

    @run_in_sync
    @with_setup_and_finish
    async def test_override_device(self):
        devices = await self._initiate_devices()
        devices[0]['locations'] = ['jd']

        response = await self.ugly_post_wrapper(SetDeviceView.url, json=devices[0])
        self.assertEqual('OK', response)

        self.assertSetEqual({b'location:co', b'location:jd'}, set(self.redis.keys('location:*')))
        self.assertSetEqual({b'device_type:drone', b'device_type:mobile'}, set(self.redis.keys('device_type:*')))

        self.assertSetEqual({b'drone_co_2', b'mobile'}, self.redis.smembers('location:co'))
        self.assertSetEqual({b'drone_co_1', b'mobile'}, self.redis.smembers('location:jd'))

        self.assertSetEqual({b'drone_co_1', b'drone_co_2'}, self.redis.smembers('device_type:drone'))
        self.assertSetEqual({b'mobile'}, self.redis.smembers('device_type:mobile'))

    @run_in_sync
    @with_setup_and_finish
    async def test_delete_device(self):
        await self._initiate_devices()

        response = await self.ugly_post_wrapper(RemoveDeviceView.url, json={'id': 'mobile'})
        self.assertEqual('OK', response)

        self.assertListEqual([b'location:co'], self.redis.keys('location:*'))
        self.assertListEqual([b'device_type:drone'], self.redis.keys('device_type:*'))

        self.assertSetEqual({b'drone_co_1', b'drone_co_2'}, self.redis.smembers('location:co'))
        self.assertSetEqual(set(), self.redis.smembers('location:jd'))

        self.assertSetEqual({b'drone_co_1', b'drone_co_2'}, self.redis.smembers('device_type:drone'))
        self.assertSetEqual(set(), self.redis.smembers('device_type:mobile'))

    @run_in_sync
    @with_setup_and_finish
    async def test_delete_non_existing_device(self):
        response = await self.ugly_post_wrapper(RemoveDeviceView.url, json={'id': 'non-existing-id'})
        self.assertEqual('OK', response)
