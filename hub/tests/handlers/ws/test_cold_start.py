import asyncio

from hub.tests.handlers.ws.test_delivery import TestCaseWithSomeDevicesAndDeliverancesDefined
from utils.aiotools import run_in_sync, sleep_until
from utils.testing import with_setup_and_finish


class MessagesForOfflineDevicesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_send_message_for_offline_device(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        # make device offline
        offline_device = self.all_receiver_devices[0]
        await offline_device.disconnect()

        await self.sender.send('debug-alert', {}, receiver_id='xafy')

        # wait for confirmation from HUB
        await sleep_until(lambda: len(self.sender.received_messages) == 1, timeout=2)

        await asyncio.sleep(.1)
        self.assertEqual(0, len(offline_device.received_messages))

    @run_in_sync
    @with_setup_and_finish
    async def test_send_message_for_online_and_offline_device(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))


# TODO: test message delivery after reconnecting
# - cold start with current stream of new messages (messages will be delivered
#   just once with correct order)
