import asyncio

from hub.handlers import SetDeliveranceView, SetDeviceView
from hub.helpers.dummy_device import DummyDevice
from hub.tests import SetHubWriterTestCase
from utils.aiotools import run_in_sync, run_cascade, sleep_until, chain
from utils.testing import with_setup_and_finish


class TestCaseWithSomeDevicesAndDeliverancesDefined(SetHubWriterTestCase):
    def setUp(self):
        super(TestCaseWithSomeDevicesAndDeliverancesDefined, self).setUp()

        self.sender = DummyDevice(self.hub, 'test_pair_1')

        self.drone_co_1 = DummyDevice(self.hub, 'test_pair_2')
        self.drone_co_2 = DummyDevice(self.hub, 'test_pair_3')
        self.drone_jd_1 = DummyDevice(self.hub, 'test_pair_4')
        self.mobile_jd = DummyDevice(self.hub, 'test_pair_5')
        self.mobile_co_jd = DummyDevice(self.hub, 'test_pair_6')

        self.all_receiver_devices = [
            self.drone_co_1,
            self.drone_co_2,
            self.drone_jd_1,
            self.mobile_jd,
            self.mobile_co_jd,
        ]

        self._setup = run_cascade(
            self._setup,
            chain(
                self._initiate_all(),
                run_cascade(
                    self.sender.connect(),
                    *[rcv.connect() for rcv in self.all_receiver_devices],
                )
            )
        )
        self._finish = run_cascade(
            *[rcv.disconnect() for rcv in self.all_receiver_devices],
            self.sender.disconnect(),
            self._finish,
        )

    async def _initiate_all(self):
        await asyncio.gather(self._initiate_deliverances(),
                             self._initiate_devices())

    async def _initiate_deliverances(self):
        deliverances = [
            {
                'message_type': 'alert',
                'device_types': ['drone', 'mobile'],
            },
            {
                'message_type': 'info',
                'device_types': ['drone', 'mobile', 'hub'],
            },
            {
                'message_type': 'healthcheck',
                'device_types': ['hub'],
            },
        ]
        for deliverance in deliverances:
            response = await self.ugly_post_wrapper(SetDeliveranceView.url, json=deliverance)
            self.assertEqual('OK', response)

    async def _initiate_devices(self):
        devices = [
            {
                'id': self.sender.device_id,
                'device_type': 'dedrone',
                'locations': ['jd', 'co'],
            },
            {
                'id': self.drone_co_1.device_id,
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': self.drone_co_2.device_id,
                'device_type': 'drone',
                'locations': ['co'],
            },
            {
                'id': self.drone_jd_1.device_id,
                'device_type': 'drone',
                'locations': ['jd'],
            },
            {
                'id': self.mobile_jd.device_id,
                'device_type': 'mobile',
                'locations': ['jd'],
            },
            {
                'id': self.mobile_co_jd.device_id,
                'device_type': 'mobile',
                'locations': ['jd', 'co'],
            },
        ]
        for device in devices:
            response = await self.ugly_post_wrapper(SetDeviceView.url, json=device)
            self.assertEqual('OK', response)


class GlobalMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_be_not_touched(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        self.assertEqual(1, len(self.sender.sent_messages))
        for recipient in self.all_receiver_devices:
            self.assertEqual(1, len(recipient.received_messages))
            self.assertEqual(self.sender.sent_messages[0].emitted, recipient.received_messages[0].emitted)

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in self.all_receiver_devices:
            self.assertEqual(1, len(recipient.received_messages))
            self.assertEqual(self.sender.device_id, recipient.received_messages[0].sender_id)

    @run_in_sync
    @with_setup_and_finish
    async def test_sender_should_not_be_recipient_too(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.drone_co_1.send('alert', {}, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in self.all_receiver_devices:
            # sender receives confirmation message
            self.assertEqual(1, len(recipient.received_messages))
            if recipient is self.drone_co_1:
                self.assertEqual(self.hub.device_id, recipient.received_messages[0].sender_id)
            else:
                self.assertEqual(self.drone_co_1.device_id, recipient.received_messages[0].sender_id)

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('healthcheck', {}, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in self.all_receiver_devices:
            self.assertEqual(0, len(recipient.received_messages))


class LocationBasedMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, location='co', _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in [self.drone_co_1, self.drone_co_2, self.mobile_co_jd]:
            self.assertEqual(1, len(recipient.received_messages))
            self.assertEqual(self.sender.device_id, recipient.received_messages[0].sender_id)

        for recipient in [self.drone_jd_1, self.mobile_jd]:
            self.assertEqual(0, len(recipient.received_messages))

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, location='jinde', _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in self.all_receiver_devices:
            # sender receives confirmation message
            if recipient is not self.sender:
                self.assertEqual(0, len(recipient.received_messages))


class DirectMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, receiver_id=self.drone_co_1.device_id, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in [self.drone_co_1]:
            self.assertEqual(1, len(recipient.received_messages))
            self.assertEqual(self.sender.device_id, recipient.received_messages[0].sender_id)

        for recipient in [self.drone_co_2, self.drone_jd_1, self.mobile_co_jd, self.mobile_jd]:
            self.assertEqual(0, len(recipient.received_messages))

    @run_in_sync
    @with_setup_and_finish
    async def test_message_should_have_no_recipients(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {}, receiver_id='whatever', _wait_for_confirmation=True)
        await self.sender.send('whatever', {}, receiver_id=self.drone_co_1.device_id, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        for recipient in self.all_receiver_devices:
            self.assertEqual(0, len(recipient.received_messages))


class DebugMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_debug_messages_are_delivered_against_validation(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('whatever', {}, receiver_id=self.drone_co_1.device_id, _wait_for_confirmation=True)
        await self.sender.send('debug-whatever', {}, receiver_id=self.drone_co_1.device_id, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        self.assertEqual(1, len(self.drone_co_1.received_messages))
        self.assertEqual('whatever', self.drone_co_1.received_messages[0].message_type)
        self.assertEqual(self.sender.sent_messages[1].emitted, self.drone_co_1.received_messages[0].emitted)
