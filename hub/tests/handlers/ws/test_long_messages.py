import asyncio

from hub.tests.handlers.ws.test_delivery import TestCaseWithSomeDevicesAndDeliverancesDefined
from utils.aiotools import run_in_sync, sleep_until
from utils.testing import with_setup_and_finish


class LongMessagesTestCase(TestCaseWithSomeDevicesAndDeliverancesDefined):
    @run_in_sync
    @with_setup_and_finish
    async def test_message_that_is_long(self):
        await sleep_until(lambda: all((d.websocket for d in self.all_receiver_devices)))

        await self.sender.send('alert', {
            "positions": [
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
                {"longitude": 9.5363381, "latitude": 9.5363381, "altitude": 9.5363381},
            ]
        }, _wait_for_confirmation=True)
        await asyncio.sleep(.1)

        self.assertEqual(1, len(self.sender.sent_messages))
        for recipient in self.all_receiver_devices:
            self.assertEqual(1, len(recipient.received_messages))
            self.assertEqual(self.sender.sent_messages[0].emitted, recipient.received_messages[0].emitted)
