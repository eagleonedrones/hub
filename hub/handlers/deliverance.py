from redis import StrictRedis

from client.input_formats import DeliveranceInput
from hub.helpers.responses import ERROR_INVALID_FORMAT, SUCCESS
from hub.helpers.view import ViewWrapper


def _remove_deliverance(message_type, redis: StrictRedis):
    del redis['message_type:{}'.format(message_type)]


class SetDeliveranceView(ViewWrapper):
    """
    Sets / updates deliverance for given message_type

    ```json
    {
        "message_type": <MessageType.id>,
        "device_types": [
            <DeviceType.id>
        ]
    }
    ```
    """
    url = '/set_deliverance'

    async def handler(self, request):
        try:
            deliverance = DeliveranceInput(
                message_type=request.json['message_type'],
                device_types=request.json['device_types'],
            )
        except KeyError:
            return ERROR_INVALID_FORMAT

        # remove every possible deliverance mention
        _remove_deliverance(deliverance.message_type, self.hub.redis)

        self.hub.redis.sadd('message_type:{}'.format(deliverance.message_type), *deliverance.device_types)
        return SUCCESS


class RemoveDeliveranceView(ViewWrapper):
    """
    Removes all related information about the device

    ```json
    {
        "message_type": <MessageType.id>,
    }
    ```
    """
    url = '/remove_deliverance'

    async def handler(self, request):
        message_type = request.json.get('message_type', None)
        if not message_type:
            return ERROR_INVALID_FORMAT

        _remove_deliverance(message_type, self.hub.redis)
        return SUCCESS
