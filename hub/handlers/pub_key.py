from sanic import response

from hub.helpers.view import ViewWrapper
from utils.crypting import export_key_pub


class PubKeyView(ViewWrapper):
    """
    Shows HUB's RSA public key to let others validate it.
    """
    url = '/public_key'
    method = 'GET'

    async def handler(self, request):
        return response.text(export_key_pub(self.hub.key_pub))
