import asyncio

import time
import websockets
from sanic.log import logger, error_logger

from client.errors import HubConnectionError
from client.handshake import hub_side_handshake
from client.input_formats import MessageToHub, MessageFromHub
from client.responses import MESSAGE_CONFIRMATION, MESSAGE_PING, MESSAGE_PONG, MESSAGE_PONG_DATA, \
    MESSAGE_UNDEFINED_CLIENT, MESSAGE_UNDEFINED_CLIENT_DATA
from hub.helpers.query import get_recipients, is_device_id_present
from hub.helpers.view import WsViewWrapper


class MessageProcessorView(WsViewWrapper):
    """
    Handles ws connection at full scope:
    - handshake
    - receiving & storing messages
    - sending unread messages on connection
    - sending new messages to online devices directly
    """
    url = '/ws'

    def __init__(self, *args, **kwargs):
        super(MessageProcessorView, self).__init__(*args, **kwargs)
        self.active_clients = {}

    async def handler(self, request, websocket):
        try:
            connection = await hub_side_handshake(self.hub, websocket)
            logger.info(f'[Websocket] new connection from client "{ connection.device_id }"')
            # bind to get new messages directly
            self.active_clients[connection.device_id] = connection

            # message from unknown device
            if not is_device_id_present(connection.device_id, self.hub.redis):
                await self.message_device(connection.device_id, MESSAGE_UNDEFINED_CLIENT, MESSAGE_UNDEFINED_CLIENT_DATA)

        except HubConnectionError:
            error_logger.exception(f'[Websocket] HubConnectionError')
            return

        try:
            async for message in connection:
                await self.process_incoming_message(message, connection.device_id)

        except websockets.ConnectionClosed:
            # unbind on disconnection
            del self.active_clients[connection.device_id]
            logger.info(f'[Websocket] client "{ connection.device_id }" closed connection')

    async def process_incoming_message(self, message: MessageToHub, sender_id):
        """
        Processes incoming messages based on their types:
        - storing client's received confirmations
        - "ponging" "ping" messages
        - broadcasting ordinary messages
            - with received confirmation reply
        """
        logger.info(f'[Websocket] message type "{ message.message_type }" from "{ sender_id }"')

        # Ping & Pong messages
        #  - not stored
        #
        if message.message_type == MESSAGE_PING:
            await self.message_device(sender_id, MESSAGE_PONG, MESSAGE_PONG_DATA)

        # Traffic
        #
        else:
            # outbound message to be (re)sent to destination device
            #
            outbound_message = MessageFromHub(message_type=message.message_type,
                                              data=message.data.plain,
                                              emitted=message.emitted,
                                              sender_id=sender_id)

            # Debug- messages
            #  - not stored
            #  - no validation for type & location for receiver
            #
            if message.message_type[:6] == 'debug-':
                if not message.receiver_id:
                    error_logger.error(f'[Websocket] debug messages need `receiver_id` defined (from client '
                                       f'"{ sender_id }")')
                    raise ValueError('debug messages need `receiver_id` defined')

                outbound_message.message_type = message.message_type[6:]
                await self._direct_message(message.receiver_id, outbound_message)

            # if read confirmation
            # else
            # broadcast client's messages

            # Broadcast messages
            #  - stored
            #  - resending to other clients (when defined)
            #
            else:
                # TODO: tests
                # only registered devices can broadcast messages
                if is_device_id_present(sender_id, self.hub.redis):
                    await self.broadcast(message.receiver_id, message.location, outbound_message)
                else:
                    await self.message_device(sender_id, MESSAGE_UNDEFINED_CLIENT, MESSAGE_UNDEFINED_CLIENT_DATA)

            # confirm received
            #
            await self.message_device(sender_id, MESSAGE_CONFIRMATION, {'emitted': message.emitted})

    async def _direct_message(self, recipient_id, message: MessageFromHub):
        if message.sender_id == recipient_id:
            # don't send message to it's sender
            return

        active_client = self.active_clients.get(recipient_id, None)
        if active_client:
            try:
                await active_client.send(message)

            except websockets.ConnectionClosed as error:
                if error.code != 1006:
                    raise error

                # remove no longer active clients
                del self.active_clients[recipient_id]

            log_message = f'[Websocket] message type "{ message.message_type }" from "{ message.sender_id }" sent to ' \
                          f'"{ recipient_id }"'
            if message.message_type == MESSAGE_CONFIRMATION:
                logger.debug(log_message)
            else:
                logger.info(log_message)

        else:
            logger.info(f'[Websocket] message type "{ message.message_type }" from "{ message.sender_id }" '
                        f'not delivered, "{ recipient_id }" is not connected')

    async def message_device(self, recipient_id, message_type, data):
        message = MessageFromHub(message_type=message_type,
                                 data=data,
                                 emitted=time.time(),
                                 sender_id=self.hub.device_id)

        await self._direct_message(recipient_id, message)

    async def broadcast(self, receiver_id, location, message: MessageFromHub):
        recipients = get_recipients(receiver_id, location, message.message_type, self.hub.redis)
        if not recipients:
            logger.info(f'[Websocket] message from "{ message.sender_id }" has no recipients')
        await asyncio.gather(*[
            self._direct_message(recipient_id, message)
            for recipient_id in recipients
        ])
