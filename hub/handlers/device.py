from redis import StrictRedis

from client.input_formats import DeviceInput
from hub.helpers.responses import ERROR_INVALID_FORMAT, SUCCESS
from hub.helpers.view import ViewWrapper


def _remove_device(device_id, redis: StrictRedis):
    # TODO: do transactions
    # remove from all location lists
    for location in redis.keys('location:*'):
        redis.srem(location, device_id)

    # remove from all device_type lists
    for device_type in redis.keys('device_type:*'):
        redis.srem(device_type, device_id)


class SetDeviceView(ViewWrapper):
    """
    Sets / updates device params

    ```json
    {
        "id": <Device.id>,
        "device_type": <DeviceType.id>,
        "locations": [
            <Location.id>
        ]
    }
    ```
    """
    url = '/set_device'

    async def handler(self, request):
        try:
            device = DeviceInput(
                device_id=request.json['id'],
                device_type=request.json['device_type'],
                locations=request.json['locations'],
            )
        except KeyError:
            return ERROR_INVALID_FORMAT

        # remove every possible device mention
        _remove_device(device.id, self.hub.redis)

        # TODO: do transactions
        self.hub.redis.sadd('device_type:{}'.format(device.device_type), device.id)
        for location in device.locations:
            self.hub.redis.sadd('location:{}'.format(location), device.id)

        return SUCCESS


class RemoveDeviceView(ViewWrapper):
    """
    Removes all related information about the device

    ```json
    {
        "id": <Device.id>,
    }
    ```
    """
    url = '/remove_device'

    async def handler(self, request):
        device_id = request.json.get('id', None)
        if not device_id:
            return ERROR_INVALID_FORMAT

        _remove_device(device_id, self.hub.redis)
        return SUCCESS
