import os
import pkg_resources
from sanic import response

import settings
from hub.helpers.view import ViewWrapper


class VersionView(ViewWrapper):
    """
    Shows versions or commit ids of HUB and related custom packages.
    """
    url = '/version'
    method = 'GET'

    async def handler(self, request):
        with open(os.path.join(settings.BASE_DIR, 'version'), 'r') as f:
            hub_version = f.read().split('\n')[0]

        utils_version = pkg_resources.require('utils')[0].version
        client_version = pkg_resources.require('client')[0].version

        return response.text(f'''
             hub commit id : { hub_version }
            client version : { client_version }
             utils version : { utils_version }
        ''')
