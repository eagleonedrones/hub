import unittest

import redis

import settings


class TestCaseWithRedis(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        self.redis = None
        super(TestCaseWithRedis, self).__init__(*args, **kwargs)

    def setUp(self):
        self.redis = redis.StrictRedis(host=settings.TEST_REDIS_HOST,
                                       port=settings.TEST_REDIS_PORT,
                                       db=settings.TEST_REDIS_DB)

    def tearDown(self):
        self.redis.flushdb()
