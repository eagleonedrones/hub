# Eagle.One's HUB

<!-- TODO: napsat anglicky a uhladit -->


HUB je komponenta zajišťující komunikaci mezi systémem DeDrone, mobilními aplikacemi uživatelů a samotnými Eagle drony (dále jen "zařízení").

**Poznámka:**
informace označené *(TODO: \<informace\>)* jsou plánované, nicméně v tuto chvíli
ještě neimplementované.



## Obecné informace

HUB momentálně běží na veřejné IP `37.157.193.224` na portu `80`.

* `/logs/hub/` - logy z běhu samotné HUB služby
* `/logs/nginx/` - logy nginx (pouze proxy)
* `/public_key/` - veřejný klíč HUBu (např. pro možnost ověření pravosti spojení)
* `/version` - aktuální verze HUBu a vlastních relevantních modulů (generováno automaticky z runtime)



## Doručování zpráv

Zprávy jsou doručovány následujícím způsobem:

1. zařízení odešle zprávu do HUBu,
2. HUB vrátí zpět potvrzení o přijetí zprávy,
3. dle typu zprávy a, volitelně, dalšího určení se vytvoří seznam příjemců (zařízení),
	* je-li příjemce online (k HUBu právě připojen), je zpráva doručena přímo,
	*  *(TODO: jinak se zpráva doručí až při prvním připojení příjemce k HUBu;)*
3. příjemce HUBu potvrdí příjem zprávy.


### Typy zpráv

Počet a varianty typů zpráv není omezený. Každý typ zprávy má definované typy zařízení, kterým je doručován. Rozhraní pro nastavení je toto:

*POST:* **/setDeliverance**

<!-- TODO: automaticky doplňovat formát z kódu -->

```javascript
{
	"message_type": "<MessageType.id>",
	"device_types": [
		"<DeviceType.id>",
		// ...
	]
}
```

*POST:* **/removeDeliverance**

```javascript
{
	"message_type": "<MessageType.id>"
}
```

### Typy zařízení

Počet a varianty typů zařízení nejsou omezeny. Každé zařízení má definovaný jeho typ a lokality, do kterých patří:

*POST:* **/setDevice**

```javascript
{
	"device_id": "<Device.id>",
	"device_type": "<DeviceType.id>",
	"locations": [
		"<Location.id>",
		// ...
	]
}
```

*POST:* **/removeDevice**

```javascript
{
	"device_id": "<Device.id>"
}
```


## Formát zpráv

Formát zpráv je typově validovaný s výjimkou vnitřní struktury atributu `data`.


### Odesílání do HUBu

```javascript
{
	// - povinně
	//
	"type": "<typ zprávy>",
	
	// - každý typ zprávy má / může mít vlastní strukturu dat
	//
	// - povinně
	//
	"data": {},
	
	// - informace o tom, kdy byla zpráva vytvořena
	// - povinně
	"emitted": <unix timestamp>,

	// - určením lokality se zprávy doručí jen zařízením, která
	//   patří do dané lokality (a následně i jen těm, která jsou
	//   typu, pro který je daný typ zpráv určený)
	//
	// - volitelně
	//
	"location": "<Location.id>",
	
	// - přímá zpráva konkrétnímu zařízení (následně se ještě validuje
	//   typ zprávy, jestli souhlasí s daným typem zařízení)
	//
	// - volitelně
	//
	"receiver_id": "<Device.id>"
}
```

### Zprávy z HUBu

```javascript
{
	"type": "<MessageType.id>", 	// - viz výše
	"data": {},                 	//
	"emitted": <unix timestamp>,	//
	
	// - čas, pod kterým je zpráva v HUBu uložená, zároveň také platí,
	//   že se jedná o čas, kdy zpráva do HUBu dorazila
	//
	"stored": <unix timestamp>,
	
	// - zařízení, které zprávu vytvořilo (tj. `emitted` je čas vzniku
	//   zprávy na daném zařízení
	//
	"sender_id": "<Device.id>"
}
```


## Připojení / handshake s HUBem

S každým zařízením, které se k HUBu připojí, musí proběhnout handshake, během kterého si zařízení vymění svůj veřejný klíč a pošlou si tímto klíčem zašifrovaný secret pro ověření identity zařízení na druhé straně. HUB má svůj veřejný klíč vystavený na adrese `{ HUB_ADDRESS }/public_key` pro možnost kontroly.

Hanshake vypadá následovně:

```sh
# výměna veřejných RSA klíčů
client >>> { CLIENT_PUBLIC_KEY }
HUB >>> { HUB_PUBLIC_KEY }

# klient se ptá na otázku a dotaz posílá už šifrovaně
client >>> { encrypt("What's the question?", HUB_PUBLIC_KEY) }

# HUB posílá secret a očekává, že klient mu odpoví (tj. dokáže si takovou odpověď rozšifrovat)
HUB >>> { encrypt("secret-some-random-value", CLIENT_PUBLIC_KEY) }
client >>> { encrypt("secret-some-random-value", HUB_PUBLIC_KEY) }

# formální "Welcome"
HUB >>> { encrypt("Welcome!", CLIENT_PUBLIC_KEY) }
client >>> { encrypt("Hi!", HUB_PUBLIC_KEY) }
```

Následující komunikace momentálně není šifrovaná veřejnými klíči opačných stran z důvodu omezené velikosti zprávy, kterou lze šifrovat.



## ./hub-cli.py

Součástí HUBu je `hub-cli.py`, jenž je rozhraní pro posílání a přijímání zpráv z HUBu. Pro účely testování a zkoušení je vhodné posílat typy zpráv s prefixem `debug-` viz níže.

Lze volat `./hub-cli.py help`, který vypíše seznam aktuálně podporovaných příkazů s ukázkami jejich použití.


### debug-

Vzhledem k faktu, že i když se zpráva odesílá s parametrem `receiver_id`, nemusí být zpráva takovému zařízení přímo doručena -- např. je-li takové zařízení jen testovací klient z `./hub-cli.py`, který nejspíše nebude pod svým ID do HUBu zaregistrovaný. Pro tyto testovací účely lze posílat zprávy v debug módu, kdy neprobíhá validace vhodnost typu zprávy pro dané zařízení -- tedy nejen že nemusí být dané zařízení registrováno, ale ani daný typ zprávy!


### Příklad: test komunikce v rámci jednoho stroje


**1. terminal1**

```
user@localhost> ./run.py
>>> [2018-12-19 11:56:09 +0100] [33829] [DEBUG] 
>>>                  ▄▄▄▄▄
>>>         ▀▀▀██████▄▄▄       _______________
>>>       ▄▄▄▄▄  █████████▄  /                 \
>>>      ▀▀▀▀█████▌ ▀▐▄ ▀▐█ |   Gotta go fast!  |
>>>    ▀▀█████▄▄ ▀██████▄██ | _________________/
>>>    ▀▄▄▄▄▄  ▀▀█▄▀█════█▀ |/
>>>         ▀▀▀▄  ▀▀███ ▀       ▄▄
>>>      ▄███▀▀██▄████████▄ ▄▀▀▀▀▀▀█▌
>>>    ██▀▄▄▄██▀▄███▀ ▀▀████      ▄██
>>> ▄▀▀▀▄██▄▀▀▌████▒▒▒▒▒▒███     ▌▄▄▀
>>> ▌    ▐▀████▐███▒▒▒▒▒▐██▌
>>> ▀▄▄▄▄▀   ▀▀████▒▒▒▒▄██▀
>>>           ▀▀█████████▀
>>>         ▄▄██▀██████▀█
>>>       ▄██▀     ▀▀▀  █
>>>      ▄█             ▐▌
>>>  ▄▄▄▄█▌              ▀█▄▄▄▄▀▀▄
>>> ▌     ▐                ▀▀▄▄▄▀
>>>  ▀▀▄▄▀
>>> 
>>> [2018-12-19 11:56:09 +0100] [33829] [INFO] Goin' Fast @ http://127.0.0.1:8000
>>> [2018-12-19 11:56:10 +0100] [33831] [INFO] Starting worker [33831]
>>> ...
```

* spuštění HUB serveru,
* do `stdout` je psán log ze serveru.


**2. terminal2**

```
user@localhost> ./hub-cli.py message-listener test_device_1
>>> creating / loading RSA keys...
>>> connecting...
>>> listening... (device_id: k/a2+Z)
>>> ...
```

* spuštění klienta poslouchající zprávy,
* `test_device_1` je alias pro RSA klíče, které se mají pro identifikaci klienta v dané session použít (pokud neexistují, vytvoří se),
* v naší session (resp. pro dané RSA klíče s aliasem `test_device_1` se vytvořilo id zařízení `k/a2+Z`, které použijeme pro odeslání zprávy.


**3. terminal3**

```
user@localhost> ./hub-cli.py send-message debug-cokoli cli_alias=device_2 receiver_id='k/a2+Z' '{"jakákoli": "zpráva"}'
>>> creating / loading RSA keys...
>>> connecting...
>>> sending...
>>> message has been successfully sent in 0.12s using   sender_id = MDnmU/
>>> {"emitted": 1545217218.899105, "type": "debug-cokoli", "data": {"jak\u00e1koli": "zpr\u00e1va"}, "receiver_id": "k/a2+Z"}
```

* pro odeslání jsme použili id zařízení z `terminal2`, tedy: `k/a2+Z`,
* zároveň vidíme id zařízení, ze kterého se zpráva odeslala: `MDnmU/`.

**4. terminal2**

```
>>> {"emitted": 1545217218.899105, "stored": 1545217218.9780328, "type": "cokoli", "data": {"jak\u00e1koli": "zpr\u00e1va"}, "sender_id": "MDnmU/"} 
```

* `emitted` je shodný s časem odesílání z původního zařízení,
* `sender_id` je shodný s id původního zařízení,
* `type` už je ve finální podobě, tzn. `debug-` prefix se použije pouze pro zpracování.


## Instalace

### Závislosti
1. *docker server* - návod zde: [https://docs.docker.com/install/linux/docker-ce/ubuntu/]()
   
    * Pro správnou funkčnost dockeru je zapotřebí přidat uživatele do skupiny docker

    ```
    user@localhost> sudo usermod -aG docker $USER.
    ```

2. *docker-compose* balíček (odzkoušeno s verzí 1.23.2)

    ```
    user@localhost> pip install docker-compose
    ```

### Instalace docker image 
1. Je zde přítomen `Makefile` s plnou podporou, tedy pro otestování, zda HUB funguje lze zavolat:

    ```
    user@localhost> make && make test
    ```

2. Pro spuštění HUBu:

    ```
    user@localhost> make && make run
    ```

Pokud používate SSH klíč jiný než *~/.ssh/id_rsa.pub* pozměnte *Makefile* tak, aby cesta vedla ke správnému klíči.
Případně lze upravit `.env` soubor, ze kterého se propaguje nastavení do `docker-compose.yaml`.

Dále je možné vytvořit `local_settings.py`, kde lze přepsat výchozí konfiguraci z `settings.py`, pokud se z nějakého důvodu nehodí přepisovat `.env`.


## FAQ

Jakékoli další dotazy prosím směřujte přímo na mě na [petr@vitecek.cz](), rád je zodpovím a ty obecnější zde uvedu i s odpovědí.













*Poslední aktualizace: 22. 12. 2018*
