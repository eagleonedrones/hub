#!/bin/bash

set -e

# Commands available using `docker-compose run hub [COMMAND]`
case "$1" in
    python)
        /venv/bin/python
    ;;
    test)
        /venv/bin/python -m unittest
    ;;
    *)
        echo "Running Server..."
        /venv/bin/python runserver.py
    ;;
esac
