import os
import sys

BASE_DIR = os.environ['BASE_DIR']


RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', os.path.join(BASE_DIR, 'keys'))
RSA_KEYS_BITS = 2048


APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = int(os.environ.get('APP_PORT', 8000))
APP_DEBUG = False
APP_AUTO_RELOAD = False


REDIS_HOST = os.environ.get('REDIS_HOST', 'redis')
REDIS_PORT = int(os.environ.get('REDIS_PORT', 6379))
REDIS_DB = int(os.environ.get('REDIS_DB', 0))


## TEST ENV
#

TEST_APP_HOST = os.environ.get('TEST_APP_HOST', APP_HOST)
TEST_APP_PORT = int(os.environ.get('TEST_APP_PORT', APP_PORT + 1))


TEST_REDIS_HOST = os.environ.get('TEST_REDIS_HOST', REDIS_HOST)
TEST_REDIS_PORT = int(os.environ.get('TEST_REDIS_PORT', REDIS_PORT))
TEST_REDIS_DB = int(os.environ.get('TEST_REDIS_DB', REDIS_DB + 1))


LOGGING_CONFIG = dict(
    version=1,
    disable_existing_loggers=False,

    loggers={
        'sanic.root': {
            'level': 'INFO',
            'handlers': ['console', 'file']
        },
        'sanic.error': {
            'level': 'INFO',
            'handlers': ['error_console', 'error_file'],
            'propagate': True,
            'qualname': 'sanic.error'
        },

        'sanic.access': {
            'level': 'INFO',
            'handlers': ['access_console', 'access_file'],
            'propagate': True,
            'qualname': 'sanic.access'
        }
    },
    handlers={
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'generic',
            'stream': sys.stdout
        },
        'error_console': {
            'class': 'logging.StreamHandler',
            'formatter': 'generic',
            'stream': sys.stderr
        },
        'access_console': {
            'class': 'logging.StreamHandler',
            'formatter': 'access',
            'stream': sys.stdout
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'generic',
            'filename': os.path.join(BASE_DIR, 'logs', 'hub', 'general.log'),
            'maxBytes': 512 * 1024,
            'backupCount': 3
        },
        'error_file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'generic',
            'filename': os.path.join(BASE_DIR, 'logs', 'hub', 'error.log'),
            'maxBytes': 512 * 1024,
            'backupCount': 3
        },
        'access_file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'access',
            'filename': os.path.join(BASE_DIR, 'logs', 'hub', 'access.log'),
            'maxBytes': 512 * 1024,
            'backupCount': 3
        },
    },
    formatters={
        'generic': {
            'format': '%(asctime)s [%(process)d] [%(levelname)s] %(message)s',
            'datefmt': '[%Y-%m-%d %H:%M:%S %z]',
            'class': 'logging.Formatter'
        },
        'access': {
            'format': '%(asctime)s - (%(name)s)[%(levelname)s][%(host)s]: ' +
                      '%(request)s %(message)s %(status)d %(byte)d',
            'datefmt': '[%Y-%m-%d %H:%M:%S %z]',
            'class': 'logging.Formatter'
        },
    }
)


# try to override this global settings with local ones
try:
    from local_settings import *
except ImportError:
    pass
