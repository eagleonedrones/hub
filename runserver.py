from redis import StrictRedis

import settings
from hub import Hub

if __name__ == '__main__':
    redis = StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)
    hub = Hub(host=settings.APP_HOST, port=settings.APP_PORT, redis=redis, log_config=settings.LOGGING_CONFIG)
    hub.start(debug=settings.APP_DEBUG, auto_reload=settings.APP_AUTO_RELOAD)
